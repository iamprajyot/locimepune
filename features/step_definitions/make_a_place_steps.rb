# When /^I insert valid coordinates$/ do
#   fill_in "place_cs", :with => "50,14"
# end

# And /^I press Create place$/ do
#   # pending
# end

Then /^place will be created$/ do
  # pending
end

And /^I will be redirected to ghash page with submitted coordinates and short url$/ do
  redirected_to "ghash page"
end


# When I fill in "place_cs" with "50,14"
# And I press "Create place"
# Then place will be created
# And I will be redirected to ghash page with submitted coordinates and short url


Given /^I am a fresh visitor on loci\.me$/ do
  # pending
end

When /^I create place from full form page with valid data$/ do
  visit new_place_path
  fill_in "place[cs]", :with => "50,14"
  click_button "Create place"
end

Then /^I should see the place page$/ do
  # response.should redirect_to(ghash_path)
  # response.url should_be(ghash_path)
  response.should have_tag("div", :class => "flash notice")
  assert_contain("Created")
  response.should have_tag("h2", :id => "ghash")
  response.should have_tag("div", :id => "map")
  # response_url should_be(ghash_path(@gitem.ghash))
  # @gitem defined ?
end
