Feature: Favorite (flagged) places
In order to improve working with a lot of places
and add popularity measurement of places
As a registered user
I can flag my or any other place (visible to me) as a favorite
to have faster access to it

  Scenario: flag my place
    Given I am logged in as a user
    And I have 1 saved place
    When I mark this place as a flagged
    Then I will see 1 flagged place
    And 1 saved place

  Scenario: flag other public place
    Given I am logged in as a user
    And I have 0 flagged places
    When I mark place of some other user as a flagged
    Then I will see 1 flagged place

  Scenario: unflag place
    Given I am logged in as a user
    And I have 1 flagged places
    When I mark this flagged place as unflagged
    Then I will see 0 flagged place
    
  Scenario: list flagged places
    Given I am logged in as a user
    And I have 1 my flagged place
    and ! have 1 other's user flagged place
    When I list my flagged places
    Then I will see 2 flagged places
    
  Scenario: place score - 1
    Given there is one place flagged by nobody
    Then it has score 0

  Scenario: place score - 2
    Given there is one place flagged once
    Then it has score 1
    
  Scenario: place score - 3
    Given there is one place flagged once by 3 users other than owner
    Then it has score 3
  
  Scenario: change accessibility of flagged place
    Given I am logged in as a user
    And I have 1 flagged place of someone else
    When someone else change the place as unaccessible for me
    Then I will see the flagged place in a listing of my flagged places without details and coordinates

  Scenario: delete flagged place
    Given I am logged in as a user
    And I have 1 flagged place of someone else
    When someone else delete the place
    Then all flags of this place will be deleted
    And I will not see this place in a listing of my flagged places
    


