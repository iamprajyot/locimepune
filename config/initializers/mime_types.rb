# Be sure to restart your server when you modify this file.

# Add new mime types for use in respond_to blocks:
# Mime::Type.register "text/richtext", :rtf
# Mime::Type.register_alias "text/html", :iphone
Mime::Type.register "application/vnd.google-earth.kml+xml", :kml
#Mime::Type.register "application/vnd.google-earth.kmz", :kmz
#Mime::Type.register "text/xml", :gpx
Mime::Type.register "application/gpx+xml", :gpx
Mime::Type.register "application/vnd.nokia.landmarkcollection+xml", :lmx
Mime::Type.register_alias "text/html", :gm
Mime::Type.register_alias "text/html", :ym