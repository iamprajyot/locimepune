set :application, "locime"
set :repository,  "git@bitbucket.org:santhoshk/locime.git"
# set :domain, 'loci.me'

set :user, "locime"

# If you aren't deploying to /u/apps/#{application} on the target
# servers (which is the default), you can specify the actual location
# via the :deploy_to variable:
set :deploy_to, "/home/locime/apps/locime"

# If you aren't using Subversion to manage your source code, specify
# your SCM below:
#set :scm, :subversion
server "192.237.217.176", :app, :web, :db, :primary => true
set :deploy_via, :remote_cache
set :scm, "git"
set :branch, 'master' 
set :scm_verbose, true 
set :use_sudo, false

# role :app, domain
# role :web, domain
# role :db,  domain, :primary => true

namespace :deploy do 
  task :restart do 
    run "touch #{current_path}/tmp/restart.txt" 
  end 
end

