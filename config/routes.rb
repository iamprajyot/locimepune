ActionController::Routing::Routes.draw do |map|
  # The priority is based upon order of creation: first created -> highest priority.
  
  map.resource          :account,
                        :controller => "users", :as => "me"
  # map.resources  :users, :shallow => true do |user|
  #   user.resources :places do |place|
  #     place.resources :comments
  #   end
  # end
  # map.resources         :users 
  #, :as => "u" NEE, /u bude user_profiles resource
  map.resource          :user_session
  map.resources         :password_resets
  
  map.root              :controller => "gitems",
                        :action => "q_new",
                        :conditions => { :method => :get }
  
  map.register          "/register",
                        :controller => "users",
                        :action => "new" 
  
  map.login             "/login",
                        :controller => "user_sessions",
                        :action => "new"

  map.q_new             "/new",
                        :controller => "gitems",
                        :action => "q_new",
                        :conditions => { :method => :get }  
  
  map.q_create          "/new",
                        :controller => "gitems",
                        :action => "q_create",
                        :conditions => { :method => :post }
  
  map.q_create_webcall  "/create",
                        :controller => "gitems",
                        :action => "q_create"
  
  map.q_create_webcall_format  "/create.:format",
                        :controller => "gitems",
                        :action => "q_create"
                          
  # tady je na :id namapovan :ghash
  map.resources         :places,
                        :requirements => { :id => /[!.~]?[a-z0-9]{4,6}/i },
                        :except => :destroy  
                          
  # zobrazeni gitemu - default
  map.ghash             "/:ghash",
                        :controller => "places",
                        :action => "show",
                        :requirements => { :ghash => /[!.~]?[a-z0-9]{4,6}/i }
                        
  map.ghash_mobile      "/m/:ghash",
                        :controller => "places",
                        # :action => "show_mobile",
                        :action => "show",
                        :view_for => "mobile",
                        :requirements => { :ghash => /[!.~]?[a-z0-9]{4,6}/i, :view_for => /mobile/ }
  
  map.ghash_format      "/:ghash.:format",
                        :controller => "gitems",
                        :action => "show",
                        :requirements => { :ghash => /[!.~]?[a-z0-9]{4,6}/i }
  
  map.pages_index       "/p", :controller => "pages", :action => "index"
  map.pages             "/p/*pagespath",
                        :controller => "pages",
                        :action => "pager"
                                                
  # =======
  # = API =
  # =======  
  
  # map.api_create        "/api/places/create",
  #                       :controller => "places",
  #                       :action => "create",
  #                       :apicall => 1  

  map.formatted_api_create "/api/places/create.:format",
                        :controller => "places",
                        :action => "create",
                        :apicall => 1  
  
  map.connect           "/*what_a_trick",
                        :controller => "pages",
                        :action => "error_404"
  
  
  
  # Sample of regular route:
  #   map.connect 'products/:id', :controller => 'catalog', :action => 'view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   map.purchase 'products/:id/purchase', :controller => 'catalog', :action => 'purchase'
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   map.resources :products

  # Sample resource route with options:
  #   map.resources :products, :member => { :short => :get, :toggle => :post }, :collection => { :sold => :get }

  # Sample resource route with sub-resources:
  #   map.resources :products, :has_many => [ :comments, :sales ], :has_one => :seller
  
  # Sample resource route with more complex sub-resources
  #   map.resources :products do |products|
  #     products.resources :comments
  #     products.resources :sales, :collection => { :recent => :get }
  #   end

  # Sample resource route within a namespace:
  #   map.namespace :admin do |admin|
  #     # Directs /admin/products/* to Admin::ProductsController (app/controllers/admin/products_controller.rb)
  #     admin.resources :products
  #   end

  # You can have the root of your site routed with map.root -- just remember to delete public/index.html.
  # map.root :controller => "welcome"

  # See how all your routes lay out with "rake routes"

  # Install the default routes as the lowest priority.
  # Note: These default routes make all actions in every controller accessible via GET requests. You should
  # consider removing the them or commenting them out if you're using named routes and resources.
  # map.connect ':controller/:action/:id'
  # map.connect ':controller/:action/:id.:format'
end
