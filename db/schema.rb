# This file is auto-generated from the current state of the database. Instead of editing this file, 
# please use the migrations feature of Active Record to incrementally modify your database, and
# then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your database schema. If you need
# to create the application database on another system, you should be using db:schema:load, not running
# all the migrations from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20090411193840) do

  create_table "flags", :force => true do |t|
    t.integer  "gitem_id",                      :null => false
    t.integer  "user_id",                       :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "gitem_type",                    :null => false
    t.boolean  "flagged",    :default => false
  end

  add_index "flags", ["gitem_type", "gitem_id", "user_id"], :name => "index_flags_on_gitem_type_and_gitem_id_and_user_id", :unique => true

  create_table "gcodes", :force => true do |t|
    t.string  "ghash",      :limit => 32, :null => false
    t.integer "gitem_id"
    t.string  "gitem_type"
  end

  add_index "gcodes", ["ghash"], :name => "index_gcodes_on_ghash"

  create_table "logerrors", :force => true do |t|
    t.string   "name"
    t.text     "vals"
    t.datetime "created_at"
  end

  create_table "places", :force => true do |t|
    t.decimal  "lat",                         :null => false
    t.decimal  "lon",                         :null => false
    t.integer  "altitude"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title",        :limit => 128
    t.text     "description"
    t.string   "link",         :limit => 256
    t.integer  "acl"
    t.integer  "user_id"
    t.string   "map_provider"
    t.integer  "zoom"
    t.string   "maptype"
    t.string   "locquery"
    t.string   "ghash",        :limit => 32,  :null => false
  end

  add_index "places", ["ghash"], :name => "index_places_on_ghash"

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "users", :force => true do |t|
    t.string   "login"
    t.string   "crypted_password"
    t.string   "password_salt"
    t.string   "persistence_token"
    t.integer  "login_count"
    t.datetime "last_request_at"
    t.datetime "last_login_at"
    t.datetime "current_login_at"
    t.string   "last_login_ip"
    t.string   "current_login_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "perishable_token",   :default => "", :null => false
    t.string   "email",              :default => "", :null => false
    t.integer  "failed_login_count"
  end

  add_index "users", ["email"], :name => "index_users_on_email"
  add_index "users", ["login"], :name => "index_users_on_login"
  add_index "users", ["perishable_token"], :name => "index_users_on_perishable_token"
  add_index "users", ["persistence_token"], :name => "index_users_on_persistence_token"

end
