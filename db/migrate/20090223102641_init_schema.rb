class InitSchema < ActiveRecord::Migration
  def self.up
    create_table "gcodes", :force => true do |t|
      t.string  "ghash",      :limit => 32, :null => false
      t.integer "gitem_id"
      t.string  "gitem_type"
    end

    add_index "gcodes", ["ghash"], :name => "index_gcodes_on_ghash"

    create_table "places", :force => true do |t|
      t.float    "lat",                        :null => false
      t.float    "lon",                        :null => false
      t.integer  "altitude"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "title",       :limit => 128
      t.text     "description"
      t.string   "link",        :limit => 256
      t.integer  "acl"
      t.integer  "user_id"
    end

    create_table "users", :force => true do |t|
      t.string   "loginname",  :limit => 24, :null => false
      t.string   "passwd",                   :null => false
      t.datetime "created_at"
      t.datetime "updated_at"
    end
    
    create_table "logbits", :force => true do |t|
      t.string   "creator"
      t.integer  "ident"
      t.text     "vals"
      t.datetime "created_at"
    end
    
  end

  def self.down
    remove_index :gcodes, :ghash
    drop_table :gcodes, :places, :users, :logbits
  end
end
