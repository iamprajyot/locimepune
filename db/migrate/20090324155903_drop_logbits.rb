class DropLogbits < ActiveRecord::Migration
  def self.up
    drop_table :logbits
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
