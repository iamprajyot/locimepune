class CreateFlags < ActiveRecord::Migration
  def self.up
    create_table :flags do |t|
      t.integer :gitem_id, :null => false
      t.integer :user_id, :null => false
      t.boolean :value, :default => false
      t.timestamps
    end
    
    add_index :flags, [:gitem_id, :user_id], :unique => true
    
  end

  def self.down
    drop_table :flags 
  end
end
