class FloatToDecimal < ActiveRecord::Migration
  def self.up
    change_column :places, :lat, :decimal, :precision => 10, :scale => 7
    change_column :places, :lon, :decimal, :precision => 10, :scale => 7
  end

  def self.down
    change_column :places, :lat, :float, :precision => 10, :scale => 7
    change_column :places, :lon, :float, :precision => 10, :scale => 7
  end
end
