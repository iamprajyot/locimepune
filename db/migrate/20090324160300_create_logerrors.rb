class CreateLogerrors < ActiveRecord::Migration
  def self.up
    create_table :logerrors do |t|
      t.string    "name"
      t.text      "vals"
      t.datetime  "created_at"
    end
  end

  def self.down
    drop_table :logerrors
  end
end

