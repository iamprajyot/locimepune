class FlagsRozsirujuODalsiHodnoty < ActiveRecord::Migration
  def self.up
    add_column :flags, :flagged,      :boolean, :default => false
    add_column :flags, :hidden,       :boolean, :default => false
    add_column :flags, :commentsopen, :boolean, :default => false
    remove_column :flags, :value
  end

  def self.down
    add_column :flags, :value, :boolean, :default => false
    remove_column :flags, :flagged
    remove_column :flags, :hidden
    remove_column :flags, :commentsopen
  end
end
