class FailedLoginCount < ActiveRecord::Migration
  def self.up
    add_column :users, :failed_login_count, :integer
    
    add_index :users, :persistence_token
  end

  def self.down
    remove_column :users, :failed_login_count
    
    drop_index :users, :persistence_token
  end
end
