class ZasePridamValueDoFlag < ActiveRecord::Migration
  def self.up
    add_column :flags, :value, :boolean, :default => false
  end

  def self.down
    remove_column :flags, :value
  end
end
