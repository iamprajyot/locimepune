class DeleteCommentsopenAndHiddenFromFlags < ActiveRecord::Migration
  def self.up
    remove_column :flags, :hidden
    remove_column :flags, :commentsopen
  end

  def self.down
    add_column :flags, :hidden,       :boolean, :default => false
    add_column :flags, :commentsopen, :boolean, :default => false
  end
end
