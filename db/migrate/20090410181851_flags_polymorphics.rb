class FlagsPolymorphics < ActiveRecord::Migration
  def self.up
    Flag.all.each { |f| f.delete }
    remove_index :flags, [:gitem_id, :user_id]
    add_column :flags, :gitem_type, :string #, :null => false
    
    Flag.all(:conditions => "gitem_type is NULL").each do |f|
      f.update_attribute(:gitem_type, '')
    end
    change_column :flags, :gitem_type, :string, :null => false
    
    remove_column :flags, :value
    add_index :flags, [:gitem_type, :gitem_id, :user_id], :unique => true
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
