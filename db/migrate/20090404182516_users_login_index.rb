class UsersLoginIndex < ActiveRecord::Migration
  def self.up
    add_index :users, :login
  end

  def self.down
    drop_index :users, :login
  end
end
