class AddGhashFieldToGitem < ActiveRecord::Migration
  def self.up
    add_column :places, :ghash, :string  #, :limit => 32, :null => false
    
    Place.all(:conditions => "ghash is NULL").each do |place|
      place.update_attribute(:ghash, place.gcode.ghash)
    end
    
    change_column :places, :ghash, :string, :limit => 32, :null => false
    
    add_index :places, :ghash
  end

  def self.down
    remove_column :places, :ghash
    
    drop_index :places, :ghash
  end
end
