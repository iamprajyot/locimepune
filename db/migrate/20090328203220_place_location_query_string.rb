class PlaceLocationQueryString < ActiveRecord::Migration
  def self.up
    add_column :places, :locquery, :string
  end

  def self.down
    remove_column :places, :locquery
  end
end
