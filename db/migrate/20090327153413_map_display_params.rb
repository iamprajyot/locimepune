class MapDisplayParams < ActiveRecord::Migration
  def self.up
    add_column :places, :map_provider, :string
    add_column :places, :zoom, :integer
    add_column :places, :maptype, :string
  end

  def self.down
    remove_column :places, :map_provider
    remove_column :places, :zoom
    remove_column :places, :maptype
  end
end
