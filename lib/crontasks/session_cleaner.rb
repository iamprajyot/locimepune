module Crontasks
  
  class SessionCleaner
    
    def self.remove_stale_sessions
      CGI::Session::ActiveRecordStore::Session.destroy_all( ['updated_at <?', 1.day.ago] )
      # CGI::Session::ActiveRecordStore has been replaced by ActiveRecord::SessionStore in rails 2.3
    end
    
  end
  
end