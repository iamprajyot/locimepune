module Loci
  module QMatcher
    
    include Geokit::Geocoders
        
    def url_parser(url, opts = {})
      rv = HashWithIndifferentAccess.new
      if opts.has_key?(:in_params)
        url.split('?')[1].split('&').each do |parpair|
          k,v = parpair.split('=')
          if val = opts[:in_params].index(k)
            rv[val] = v
          end
        end
      end
      return rv
    end
    
    
    
    def parse_loc_query_string(q)
      q.strip!
      gitem_type = nil
      gitem_hash = HashWithIndifferentAccess.new

      case q
        
      when /^[\s.,:;-]*$/
        return false

      when /^([NSWEnswe\d\s°\'\".,;]+)(?:\((.*))?$/
        gitem_type = :place
        gitem_hash[:cs] = $1
        gitem_hash[:title] = $2.sub(/\)$/,'') if $2
        gitem_hash[:locquery] = q
        return gitem_type, gitem_hash

      when /^http:\/\/maps\.google\.[^\/]+\//
        gitem_type = :place
        gitem_hash = url_parser(q, :in_params => {:locquery => "q",
                                                  :coords   => "ll",
                                                  :zoom     => "z",
                                                  :maptype  => "t"})
        
        gitem_hash[:locquery] = URI.decode(gitem_hash[:locquery]).gsub('+',' ') if gitem_hash[:locquery]

        if gitem_hash[:locquery]
          res=Geokit::Geocoders::GoogleGeocoder.geocode gitem_hash[:locquery]
          if res.success? && res.ll
            gitem_hash[:coords] = res.ll
            # gitem_hash[:addr] = res.to_hash
            # tady casem mozna ulozeni cele adresu geocodovane, nebo aspon city, country a zip...?
          end
        end
        
        gitem_hash[:map_provider] = "gm"
        return gitem_type, gitem_hash

      else    # trying geocode as address
        q =~ /^(.*)\((.*)\)\s*$/
        if $2
          q = $1
          gitem_hash[:title] = $2
        end
        unless q.blank?
          res=Geokit::Geocoders::GoogleGeocoder.geocode q
          if res.success? && res.ll
            gitem_type = :place
            gitem_hash[:locquery] = q
            gitem_hash[:coords] = res.ll
            # gitem_hash[:addr] = res.to_hash
            return gitem_type, gitem_hash
          end    
        end
        return false
        
      end
    end
        
    
  end
end