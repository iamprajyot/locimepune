module Loci
  module QParser
    
    require 'loci/HTML_entities'
    include Loci::HTMLEntities
    
    # =================================================================

    def q_is_coords_try(params)
      if params[:q] =~ /^([\d\s,;\.°\'\"EWSN\-\+]+)(?:\s*([:(].*))*$/i
        # pro poznani multipoint a nebo routy musim predelat tenhle regexp
        
        title = $2
        placehash, coords_type = parse_coords($1)     # TODO: SOMETIMES: nezaznamenavam coords_type
        if placehash                                  # podarilo se parsnout
          params.deep_merge!(placehash)
          params[:title] ||= parse_q_title(title)
          # pars_t[:extended_data] = {:q_type => "coords", :coords_type => coords_type }
          return "Place".constantize
        end
      end
      return nil                                      # neni to coords
    end
    
    
    def q_is_multipoint_try(params)
      # TODO: SOMETIME: multipoint not yet implemented
      return nil
    end
    
    
    def q_is_url_try(params)
      if params[:q] =~ /^https?:\/\//i
        # tady bude nejen url, ze ktere se neco bude stahovat
        # pars[:extended_data] = {:q_type => "url" }
        # ale i url treba na gmaps, ze ktere pozname souradnice a pripadne title
        placehash, title, url_type = _q_parse_url(params)
        if placehash
          params.deep_merge!(placehash)
          params[:title] = title if title && params[:title].blank?
          debugger
          return "Place".constantize
        end
      end
      return nil
    end
    

    def q_is_address_try(params)
      # TODO: SOMETIME: address not yet implemented
      return nil
    end
    
        
    def _q_parse_url(params)
      # placehash, title, url_type    TODO: tohle je hnusne napsany... jen pro vyzkouseni!!!
      q = params[:q].dup
      q =~ /^https?:\/\/(.*)$/i
      q = $1
      case q
      # http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=Tower+Bridge,+london&sll=50.129382,14.420929&sspn=0.496522,1.149445&ie=UTF8&ll=51.50348,-0.076582&spn=0.007226,0.01796&z=16&iwloc=addr
      when /^maps.google.(?:.+)\?(.*)/i
        p = $1
        pd = decode_entities(p)
        p = HashWithIndifferentAccess.new
        pd.split('&').collect{|keypair|
          k,v = keypair.split('=')
          p[k] = v
        }
        lat, lon = p[:ll].split(',') if p[:ll]
        if lat && lon
          lat = lat.to_f
          lon = lon.to_f
          if p[:q]
            tit = p[:q].gsub(/\+/,' ')
            tit = URI.decode(tit)
          end
          return { :lat => lat, :lon => lon}, tit, 'google maps'
        end
      end
      return nil
    end
    
    
    def parse_q_title(title = nil)
      if title
        title.strip!
        if  title =~ /^\((.*)\)$/ then title = $1
        elsif title =~ /^\:(.*)$/ then title = $1
        end
      end
      return title
    end
    
    
  end
end

include Loci::QParser
