$: << (File.dirname(__FILE__) + '/..')

require File.expand_path(File.dirname(__FILE__) + "/../../../config/environment")


require 'q_parser'
require 'test/unit'

include Loci::QParser

class TestQParser < Test::Unit::TestCase
  
  @@tits = {    ':titulek' => 'titulek', 
                '   :titulek a jeste neco ' => 'titulek a jeste neco', 
                ' :titule (koci)' => 'titule (koci)', 
                '(nejak :y tit)' => 'nejak :y tit',
                '   (nejak :y tit)   ' => 'nejak :y tit',
                ':52,24  :y tit)' => '52,24  :y tit)'
          }
  @@coords_ok = [ ['52.12345 14.45678', 52.12345, 14.45678],
                  ['-52.1234567 -14.45678', -52.123457, -14.45678],
                  ['-52.18344567, -14.45678', -52.183446, -14.45678],
                  ['-52.1234567,-19.45678', -52.123457, -19.45678],
                  ['52.12345; -14.45678', 52.12345, -14.45678],
                  ['52.12345 -14', 52.12345, -14],
                  ['-52 14', -52, 14],
                  ['N 50° 31.080 E 015° 04.033', 50.518, 15.067217],
                  ['     -52.9395 14     ', -52.9395, 14],
                  ['N 50° 31.080, E 016° 04.033', 50.518, 16.067217],
                  ['     -52.9395,18     ', -52.9395, 18],
                  ['N51° 31.080 E015° 04.033', 51.518, 15.067217],
                  ['N52° 31.080 ; E015° 04.033', 52.518, 15.067217],
                  ['53° 31.080 015° 04.033', 53.518, 15.067217],
                  ['54° 31.080 N 015° 04.033 E', 54.518, 15.067217],
                  ['S 55° 31.080 W 015° 04.033', -55.518, -15.067217],
                  ['N 56° 31.080\' E 015° 04.033\'', 56.518, 15.067217],
                  ['N 57° 31   E 015° 4', 57.516667, 15.066667],
                  ['N 58°31   E 015°4', 58.516667, 15.066667],
                  ['N 59° 31\'   E 015° 4\'', 59.516667, 15.066667],
                  ['N 60° W 15°', 60.0, -15.0],
                  ['N 61° 31\' 4.8" E 015° 04\' 1.98"', 61.518, 15.067217],
                  ['N 62°31\'4.8" E 015°04\'1.98"', 62.518, 15.067217],
                  ['63°31\'4.8" N 15°04\'1.98" E', 63.518, 15.067217],
                  ['63°31\'4.8" N, 9°04\'1.98" E', 63.518, 9.067217],
                  ['63°31\'4.8"N,22°04\'1.98"E', 63.518, 22.067217],
                  ['64°31\'4.8" 15°04\'1.98"', 64.518, 15.067217],
                  ['N65°31\'4.8" E15°04\'1.98"', 65.518, 15.067217],
                  ['N 66° 4.8" E 015° 1.98"', 66.001333, 15.00055],
                  ['N 67° 4" E 015° 1"', 67.001111, 15.000278],
                  ['68°31\'4.8" S 15°04\'1.98" W', -68.518, -15.067217],
                ]
  @@coords_nook = ['52.123..45 14.45678',
                  'ahoj',
                  'N 50.1° 31\' 4.8" E 50° 04\' 1.98"',
                  'N 50° 31\' 4.8" E 50° 04,5\' 1.98"',
                  'N 50° 31\' .8" E 500° 04\' 1.98"',
                  'N 50° 31\' 4.8" H 015° 04\' 1.98"',
                  'N -50° 31\' 4.8" E 015° 04\' 1.98"',
                  'N50.1° 31.080 E015° 04.033',
                  'N50° 31.080 E015,6° 04.033',
                  'N50° 31.080 E015° 04.033 neco',
                  'N -50° 31.080 E015° 04.033',
                  ]
  
  # Jeste nemam implementovane to, aby se vracel typ...
  # @@qs = [  ['52 14 :title', 'coords'],
  #           ['52,14|43.23,75.23|-12.4,-20.75644', 'multiplace'],
  #           ['http://someurl', 'url'],
  #         ]
  
  
  def test_parse_ll
    ll = "52,14"
    pl, typ = parse_ll(ll)
    pla = HashWithIndifferentAccess.new(:lat => 52, "lon" => 14)
    assert_equal pla, pl
    assert_equal "WGS84 decimal", typ
  end
  
  def test_coords_formats
    @@coords_ok.each do |qs|
      par = HashWithIndifferentAccess.new(:q => qs[0])
      gtc, typ = parse_q!(par)
      assert_equal "Place".constantize, gtc
      assert_equal qs[1], par[:lat]
      assert_equal qs[2], par[:lon]
    end
  end
  
  def test_coords_bad_formats
    @@coords_nook.each do |qs|
      par = HashWithIndifferentAccess.new(:q => qs)
      gtc, typ = parse_q!(par)
      assert_nil gtc
    end
  end
  
  def test_coords_with_title
    @@coords_ok.each do |qs|
      # par = HashWithIndifferentAccess.new(:q => qs[0])
      @@tits.each_pair do |titi, tito|
        part = HashWithIndifferentAccess.new(:q => qs[0].dup)
        part[:q] << titi
        gtc, rtc = parse_q!(part)
        assert_equal "Place".constantize, gtc
        assert_equal qs[1], part[:lat]
        assert_equal qs[2], part[:lon]
        assert_equal tito, part[:title]
      end
    end
  end
  
  def test_coords_with_title_bad
    @@coords_nook.each do |qs|
      @@tits.each_pair do |titi, tito|
        part = HashWithIndifferentAccess.new(:q => qs.dup)
        part[:q] << titi
        gtc, rtc = parse_q!(part)
        assert_nil gtc
      end
    end
  end

  def test_url_with_title
      par = HashWithIndifferentAccess.new(:q => 'http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=Tower+Bridge,+london&sll=50.129382,14.420929&sspn=0.496522,1.149445&ie=UTF8&ll=51.50348,-0.076582&spn=0.007226,0.01796&z=16&iwloc=addr')
      gtc, rtc = parse_q!(par)
      assert gtc
      assert_equal 51.50348, par[:lat]
      assert_equal -0.076582, par[:lon]
      assert_equal 'Tower Bridge, london', par[:title]
  end

  
end
