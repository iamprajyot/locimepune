# passenger restart
# http://duckpunching.com/passenger-mod_rails-for-development-now-with-debugger
# add this rake file to your lib/tasks folder
# rake restart DEBUG=true
# rdebug -c
desc "Restart through passenger w/o debug mode"
task :restart do
  system("touch tmp/restart.txt")
  system("touch tmp/debug.txt") if ENV["DEBUG"] == 'true'
end

