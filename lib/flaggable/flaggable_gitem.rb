module Flaggable
  module FlaggableGitem
  
    # Budeme predpokladat, ze existuje funkce current_user
  
    # ziska hodnotu (true/false)
    def flagged
      ffu = flag_for_user
      return ( ffu ? ffu.flagged : false )
    end
  
    # nastavi hodnotu (true ... vytvori, false ... smaze)
    def flagged=(val)
      ffu = flag_for_user
      if ffu && !val
        Flag.destroy(ffu.id)
        # flag_for_user(true)
      elsif ffu && val
        # nothing ... nastavuji na true hodnotu true
        true
      else
        current_user = User.find_by_login('dadcz')  # PRO TESTOVANI
        self.flags.create(:user_id => current_user, :flagged => true)
        # flag_for_user(true)
      end
    end
  
    # ziska hodnotu pro daneho uzivatele
    def flagged_by_user(user)
      # TODO: flagged_by_user
    end
  
    # kolik ma celkem flagu od ruznych uzivatelu
    def flagged_count
      self.flags.size
    end
  
    # vylistuje vsechny flagged gitemy, defaultne podle score sestupne, bude paginovat
    def self.find_flagged
      # TODO: find_flagged - vsechny flagnuty
    end
    
  protected
  
    def flag_for_user
      current_user = User.find_by_login('dadcz')  # PRO TESTOVANI
      return nil unless current_user
      self.flags.find(:first, :conditions => {:user_id => current_user})
    end
    
  
  end
end