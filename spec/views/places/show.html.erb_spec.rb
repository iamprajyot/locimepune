require File.expand_path(File.dirname(__FILE__) + '/../../spec_helper') 

describe "places/show.html.erb" do 
  before(:each) do 
    @gitem = Place.new(:coords => "50,14")
    assigns[:gitem] = @gitem
  end 
  
  it "should display the place" do 
    render "places/show.html.erb" 
    response.should contain("Created") 
  end 
  
end 
