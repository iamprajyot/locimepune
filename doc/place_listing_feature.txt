Feature: Private places (gitems) listing
  In order to list my previously submitted and saved places
  As a loci.me user (subscriber)
  I want to list places (gitems) I have submitted
  
  Scenario:
    When I click on "my places" in the menu
    Then I get to the page where is listing of all my places.
    
  
URL: my/places  (my/locies)

Info to show:
type, title, address(state/town)
  sorting: by update data, descending
  mouseover: to display fast info
  checkbox: to do more actions with it
  
Search between them:
  fulltext (title, decription)
  on the BBox (from map)
  in the neighbourhood of another place

Gitem.find(:all, :user => current_user) ... sorted by 

