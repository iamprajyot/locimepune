require 'test_helper'

class GcodeTest < ActiveSupport::TestCase

  def test_new_create_automatic_ghash
    gc = Gcode.new(:gitem_id => 1, :gitem_type => "Place")
    assert gc.valid?
    assert_not_nil gc.ghash
  end
  
  def test_new_create_explicit_ghash
    gc = Gcode.new(:ghash => "kjkdfjks", :gitem_id => 1, :gitem_type => "Place")
    assert gc.valid?
    assert_not_nil gc.ghash
  end

  def test_new_create_explicit_ghash_fail
    nook = [  'a' * (Gcode::GHASH_MIN_LENGTH - 1),
              'a' * (Gcode::GHASH_MAX_LENGTH + 1),
              "asd.ssd",
              "aasddad_saksdkjas",
              "lasklasř",
              "kasdjkasjd kajsdkjask",
            ]
    nook.each do |ghash|
      gc = Gcode.new(:ghash => ghash, :gitem_id => 1, :gitem_type => "Place")
      assert !gc.valid?, "#{ghash}"
    end
  end
  
  def test_new_create_duplicity_ghash
    Gcode.new(:ghash => "ahojky", :gitem_id => 993, :gitem_type => "Place").save!
    gc2 = Gcode.new(:ghash => "ahojky", :gitem_id => 12, :gitem_type => "Place")
    assert !gc2.valid?
  end

  def test_new_create_with_gitem_id
    gc = Gcode.new(:ghash => "kjkdfjks")
    gc.gitem_id = 1
    assert !gc.valid?
  end

  def test_new_create_with_gitem_id_and_type
    gc = Gcode.new(:ghash => "kjkdfjks")
    gc.gitem_id = 1
    gc.gitem_type = "Place"
    assert gc.valid?
  end
  
  def test_new_create_with_bad_gitem_type
    gc = Gcode.new(:ghash => "kjkdfjks")
    gc.gitem_id = 1
    gc.gitem_type = "Hulu"
    assert !gc.valid?
  end


end
