require 'test_helper'

class PlaceTest < ActiveSupport::TestCase
  
  def test_rounding
    place = Place.new(:lat => 12.3456789, :lon => -88.7654321)
    place.valid?
    place.save
    assert_equal 12.345679, place.lat
    assert_equal -88.765432, place.lon
  end
  
  def test_title
    ok = ["Spravny title", "Dokonce i česky - +ěščřžýáíéúů",
          'a' * (Place::MAX_TITLE_LENGTH + 1)]
    ok.each do |tit|
      place = Place.new(:title => tit, :lat => 52, :lon => 14)
      assert place.valid?, "Title: #{tit} not allowed: #{place.errors.full_messages}"
    end
  end
  
  def test_truncate_title
    place = Place.new(:title => 'š' * (Place::MAX_TITLE_LENGTH + 1), :lat => 52, :lon => 14)
    place.valid?
    assert_equal ('š' * (Place::MAX_TITLE_LENGTH - 3)) << '...', place.title
  end
  
  def test_valid_latlon
    ok = [[0,0],[10,-10],[90,180],[-90.0,-180.0],[-12.23535645,23.23453656323]]
    ok.each do |ll|
      place = Place.new(:lat => ll[0], :lon => ll[1])
      assert place.valid?
    end
  end

  def test_notvalid_latlon
    nook = [[nil,0],[0,nil],[90.1,180],[-90.0,-180.1],[nil,nil], ["ahoj",10], [10, "ahoj"]]
    nook.each do |ll|
      place = Place.new(:lat => ll[0], :lon => ll[1])
      assert !place.valid?, "ll: #{ll.join(' -- ')}"
    end
  end

  def test_accessibility_restriction
    par = {:lat => 52, :lon => 14, :title => "title", :description => "popisek", :link => "http://somewhere", :altitude => 10, :user_id => 1, :acl => 1}
    place = Place.new(par)
    assert_nil place.user_id
    assert_equal 10, place.altitude
    assert_equal "popisek", place.description
    assert_equal "http://somewhere", place.link
    place.user_id = 1
    assert_equal 1, place.user_id
  end
  
  # def test_gcode_creation
  #   par = {:lat => 52, :lon => 14, :title => "title", :description => "popisek", :altitude => 10}
  #   place = Place.new(par)
  #   assert place.valid?
  #   assert place.save!
  #   assert place.gcode
  #   assert_equal place.gcode.ghash, place.ghash
  # end
  
  def test_existing
    pl = Place.new(:lat => 50, :lon => 14, :title => "titulek", :description => "popisek", :acl => 1)
    pl.build_user(:loginname => "huhulu", :passwd => "secret")
    pl.save!
    par = HashWithIndifferentAccess.new(:place => HashWithIndifferentAccess.new(:lat => 50, :lon => 14, :title => "titulek", :description => "popisek", :acl => 1))
    pl2 = Place.create_or_get_existing(par, pl.user)
    assert_equal pl.id, pl2.id
  end
  
  # def test_are_there_geo_params_for_me
  #   par = HashWithIndifferentAccess.new( :lat => 52, :lon => 14, :title => "title", :description => "popisek", :altitude => 10)
  #   assert Place.are_there_geo_params_for_me?(par)
  # end
  # 
  # def test_are_there_geo_params_for_me_no
  #   par = HashWithIndifferentAccess.new( :lon => 14, :title => "title", :description => "popisek", :altitude => 10)
  #   assert_nil Place.are_there_geo_params_for_me?(par)
  # end
  # 
  # def test_are_there_geo_params_for_me2
  #   par = HashWithIndifferentAccess.new( :ll => "52.234,14.344", :title => "title", :description => "popisek")
  #   assert Place.are_there_geo_params_for_me?(par)
  # end
  # 
  # def test_are_there_geo_params_for_me2_no
  #   par = HashWithIndifferentAccess.new( :title => "title", :description => "popisek")
  #   assert_nil Place.are_there_geo_params_for_me?(par)
  # end
  # 
  # def test_process_my_geo_params
  #   par = HashWithIndifferentAccess.new( :lat => 52, :lon => 14, :title => "title", :description => "popisek", :altitude => 10)
  #   Place.process_my_geo_params!(par)
  #   assert_equal 52, par[:place][:lat]
  #   assert_equal "title", par[:place][:title]
  # end
  # 
  # def test_process_my_geo_params2
  #   par = HashWithIndifferentAccess.new( :ll => "52.234,14.344", :title => "title", :description => "popisek")
  #   Place.process_my_geo_params!(par)
  #   assert_equal 52.234, par[:place][:lat]
  #   assert_equal 14.344, par[:place][:lon]
  # end
  
  # def test_coords
  #   par = HashWithIndifferentAccess.new( :lat => 52, :lon => 14, :altitude => 10)
  #   p = Place.new(par)
  #   assert "52,14,10", p.coords
  #   par1 = HashWithIndifferentAccess.new( :lat => 52, :lon => 14)
  #   p1 = Place.new(par1)
  #   assert "52,14", p.coords
  # end
  
  
#  def test_identify_and_fillin_sended_param
#    par = HashWithIndifferentAccess.new(:title => "titulek", :description => "popis", :lat => 52, :lon => 14)
#    rv = Place.identify_and_fillin_sended_params!(par)
#    assert_equal HashWithIndifferentAccess, rv.class
#    assert_equal par[:place], rv
#    assert_equal 52, par[:place][:lat]
#    assert_equal 14, par[:place][:lon]
#    assert !par[:place].key?(:altitude)
#  end
#
#  def test_identify_and_fillin_sended_param2
#    par = HashWithIndifferentAccess.new(:lat => 52, :lon => 14,
#                                        :place => HashWithIndifferentAccess.new("lat" => 22))
#    rv = Place.identify_and_fillin_sended_params!(par)
#    assert_equal HashWithIndifferentAccess, rv.class
#    assert_equal par[:place], rv
#    assert_equal 52, par[:place][:lat]
#    assert_equal 14, par[:place][:lon]
#  end
#
#  def test_identify_and_fillin_sended_param3
#    par = HashWithIndifferentAccess.new(:title => "some")
#    rv = Place.identify_and_fillin_sended_params!(par)
#    assert !rv
#    assert !par.key?(:place)
#  end
    
end
