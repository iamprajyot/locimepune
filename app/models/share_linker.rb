class ShareLinker
  
  # toto zatim pracuje pouze s Place
  
  def initialize(gitem)
    @gitem = gitem
  end
  
  def facebook
    "http://www.facebook.com/sharer.php?u=#{@gitem.locilink_escaped}"
  end
  
  def twitter
    # add title
    "http://twitter.com/home/?status=#{@gitem.title.blank? ? '' : @gitem.title + ", " }#{@gitem.locilink_escaped}"
  end
  
  def gm(view_for=nil)
    zoom = @gitem.zoom || GOOGLE_MAPS_DEFAULT_ZOOM
    maptype = @gitem.maptype.blank? ? '' : "&t=#{@gitem.maptype}"
    if view_for == "mobile"
      "http://maps.google.com/maps?f=q&q=#{@gitem.lat},#{@gitem.lon}%20(#{ !@gitem.title.blank? ? "#{URI.escape(@gitem.title)}" : "Loci.me: #{@gitem.locilink_escaped}" })&z=#{zoom}#{maptype}&"
    else
      "http://maps.google.com/maps?f=q&q=#{@gitem.locilink_escaped}.kml&z=#{zoom}#{maptype}"
      # http://maps.google.com/maps?f=q&q=http://loci.me/t341cf.kml&z=15
    end
  end
    
  def ym(view_for=nil)
    "http://maps.yahoo.com/?lat=#{@gitem.lat}&lon=#{@gitem.lon}"
  end
  
  
end