class Notifier < ActionMailer::Base
  default_url_options[:host] = "loci.me"

  def password_reset_instructions(user)
    subject       "Loci.me: Password Reset Instructions"
    from          "Loci.me notifier <noreply@loci.me>"
    # Return-Path: <sentto-21671391-17-1238798464-david.cizek=gmail.com@returns.groups.yahoo.com>
    # sender        "noreply@loci.me"
    # reply_to      "noreply@loci.me"
    recipients    user.email
    sent_on       Time.now
    body          :edit_password_reset_url => edit_password_reset_url(user.perishable_token)
  end
  
end