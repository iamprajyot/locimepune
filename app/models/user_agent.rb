class UserAgent
  
  def initialize(req)
    @request = req
  end

  def is_mobile?
    browser_type == "mobile"
  end

  def browser_type
    # return :iphoneipod # just for testing
    @browser_type ||= type_browser[0]
  end
  
  def browser_family
    @browser_family ||= type_browser[1]
  end
    
private

  def type_browser
    # return "mobile", "iphoneipod" #... just for testing
    # return "mobile", "android" #... just for testing
    # return "mobile", "nokia" #... just for testing
    @browser_type, @browser_family =  case @request.env['HTTP_USER_AGENT']
                                      when /(Mobile\/.+Safari)/
                                        ["mobile", "mobilesafari"]
                                      when /Android/
                                        ["mobile", "android"]
                                      when /Nokia|es61i|SymbianOS/
                                        ["mobile", "nokia"]
                                      when /BlackBerry/
                                        ["mobile", "mobilegeneric"]
                                      when /SonyEricsson|MOT|MIDP|CLDC|UP\.Browser|DoCoMo|Opera.Mini|LGE|samsung/i
                                        ["mobile", "mobilegeneric"]
                                      when /Windows.CE/
                                        ["mobile", "windowsmobile"]
                                      else
                                        ["default", "default"]
                                      end
  end
  
end