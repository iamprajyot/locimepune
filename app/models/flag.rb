class Flag < ActiveRecord::Base
  
  belongs_to  :gitem, :polymorphic => true
              
  belongs_to  :user
  
  validates_presence_of :gitem_id
  validates_presence_of :gitem_type
  validates_presence_of :user_id
  
  
end
