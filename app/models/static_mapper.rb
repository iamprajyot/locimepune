class StaticMapper < Mapper
  
  # GOOGLE_API_KEY = "ABQIAAAA4Vf5EDIuOh5XlkpG3USalRSiHWt5jnmHToXVoD0t91ez0BPNORSJ4DpJvBNj3VSeh_dH3pcktP2Ppg"
  # case RAILS_ENV
  # when "development"
  #   GOOGLE_API_KEY = "ABQIAAAA4Vf5EDIuOh5XlkpG3USalRSiHWt5jnmHToXVoD0t91ez0BPNORSJ4DpJvBNj3VSeh_dH3pcktP2Ppg"
  # else
  #   GOOGLE_API_KEY = "ABQIAAAA4Vf5EDIuOh5XlkpG3USalRR0GCP64JNyd7Xl2pg_YygaojG0TxT-CE0q6J6beQOv-IHnuu6LcSCuvw"
  # end
  
  GOOGLE_MAP_TYPES_MAPPING = {"m" => nil, "k" => "satellite", "h" => "hybrid", "p" => "terrain" }
  
  DEFAULTS = {:width => 478, :height => 320, :zoom => GOOGLE_MAPS_DEFAULT_ZOOM}
  
  BROWSER_DEFAULTS = {
    "default"        => {},
    "iphoneipod"     => {:width => 300, :height => 260},
    "android"        => {:width => 300, :height => 260},
    "mobilegeneric"  => {:width => 200, :height => 160},
    "nokia"          => {:width => 230, :height => 180},
    "windowsmobile"  => {:width => 230, :height => 180}
  }
  
  attr_accessor :mapper, :gitem
  
  def initialize(mapper)
    @mapper = mapper
    @gitem = mapper.gitem
  end
  
  def display(options = {})
    merge_options(options)
    buf = ''
    # buf << "http://maps.google.com/staticmap"
    # buf << "?markers=#{mapper.gitem.lat},#{mapper.gitem.lon},midorange"
    # buf << "&maptype=#{options[:maptype]}" unless options[:maptype].blank?
    # buf << "&zoom=#{options[:zoom]}"
    # buf << "&size=#{options[:width]}x#{options[:height]}"
    # buf << "&key=#{GOOGLE_API_KEY}"
    # buf << "&sensor=false"
    buf << "http://maps.googleapis.com/maps/api/staticmap"
    buf << "?center=#{mapper.gitem.lat},#{mapper.gitem.lon}"
    buf << "&markers=color:orange|label:S|#{mapper.gitem.lat},#{mapper.gitem.lon}"
    buf << "&maptype=#{options[:maptype]}" unless options[:maptype].blank?
    buf << "&zoom=#{options[:zoom]}"
    buf << "&size=#{options[:width]}x#{options[:height]}"
    buf << "&sensor=false"
  end

  def mapsize(options = {})
    merge_options(options)
    return options[:width], options[:height]
  end
  
private

  def merge_options(options)
    # options.reverse_merge!(:maptype => GOOGLE_MAP_TYPES_MAPPING[self.gitem.maptype], :zoom => self.gitem.zoom)
    options[:maptype] ||= GOOGLE_MAP_TYPES_MAPPING[self.gitem.maptype] if self.gitem.maptype
    options[:zoom] ||= self.gitem.zoom if self.gitem.zoom
    if options[:browser_family] && BROWSER_DEFAULTS[options[:browser_family]]
      options.reverse_merge!(BROWSER_DEFAULTS[options[:browser_family]])
    end
    options.reverse_merge!(DEFAULTS)    
  end
  
end