class Gcode < ActiveRecord::Base
  
  GHASH_MIN_LENGTH = 4
  GHASH_MAX_LENGTH = 6
    
  belongs_to              :gitem, :polymorphic => true

  validates_presence_of   :ghash
  validates_length_of     :ghash,
                          :in => GHASH_MIN_LENGTH..GHASH_MAX_LENGTH,
                          :message => "between #{GHASH_MIN_LENGTH} - #{GHASH_MAX_LENGTH} chars."
  validates_uniqueness_of :ghash
  validates_format_of     :ghash,
                          :with     => %r{^[!~.][0-9a-z\-]+$}i,
                          :message  => 'only alphanum or -'
                          
  validates_inclusion_of  :gitem_type,
                          :in => Gitem::AVAILABLE_GITEMS,
                          :message => "Unknown gitem type."

  def before_validation
    self.fill_in_ghash
  end


  def self.find_gitem_by_ghash(gh)
    Gcode.find_by_ghash(gh).gitem
  end

  def as_url
    "http://loci.me/#{ghash}"
  end

  def as_url_escaped
    "http%3A%2F%2Floci.me%2F#{ghash}"
  end
  
  


protected

  # Klicova fce pro generovani unikatniho ghashe - kodu
  # pouzivame sadu znaku: '12345 6789a bcdef ghjkm npqrs tuvwx yz' ... 32 znaku sada (chybi iIoO0)
  # (case insensitive)
  # zatim uvazuju o 5-6 mistnem kodu, case insensitive
  # 1 073 741 824 moznosti ... pro 6 znaku retezec
  # 33 554 432 ... 5 znaku
  # 1 048 576 ... 4 znaky
  # protoze ale chceme mit minimalne 5 znaku zatim, tak to musi byt minimalne cislo 33 554 432 a vyssi
  # takze od te plne skaly odecitam to minimum a pak k rand vysledku to minimum prictu
  
  # nove 4-5 znaku
  # takze 33554432-1048576 = 32505856
  
  # dal pak pres fci to_s(alphabet) udelam tu konverzi a pak prekoduju na moji upravenou sadu pres tr

  # mozna pouzit http://api.rubyonrails.org/classes/ActiveSupport/SecureRandom.html

  # TODO :predelat generovani ghashe, pres secure random, az budu pouzivat ruby 1.9
  def fill_in_ghash
    if self.ghash.blank?
      # need to generate
      [1..6].each do
        # gc = (rand(1040187392) + 33554432).to_s(32).tr!('0123456789abcdefghijklmnopqrstuv', '123456789abcdefghjkmnpqrstuvwxyz')
        gc = '.' + (rand(32505856) + 1048576).to_s(32).tr!('0123456789abcdefghijklmnopqrstuv', '123456789abcdefghjkmnpqrstuvwxyz')
        unless Gcode.exists?(:ghash => gc)  #find_by_ghash(gc)
          # mam ho, koncim
          self.ghash = gc
          return 
        end
        # tady zkusim randomizovat case znaku ... casem
      end
      # nepovedlo se ani na potreti vygenerovat ghash - asi vzdavam...
      return nil
    end
  end
  
  
end
