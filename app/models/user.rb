class User < ActiveRecord::Base
  
  USERNAME_MIN_LENGTH = 4
  USERNAME_MAX_LENGTH = 20
  
  acts_as_authentic do |c|
    c.crypto_provider = Authlogic::CryptoProviders::BCrypt
    c.validate_email_field = false
    c.validates_length_of_login_field_options = :login_field_type == :email ? {:within => 6..40} : {:within => USERNAME_MIN_LENGTH..USERNAME_MAX_LENGTH}
    c.validates_format_of_login_field_options = :login_field_type == :email ? {:with => standard_email_regex, :message => "should look like an email address"} : {:with => %r{^[0-9a-z\-]+$}i, :message => "should use only letters, numbers or '-'"}
    c.validates_uniqueness_of_login_field_options = {:allow_blank => false} # dokud nebudeme mit openID
    c.disable_perishable_token_maintenance = true
    c.perishable_token_valid_for = 30.minutes
  end  
  
  include Flaggable::UserCanFlag
     
  has_many            :places
  has_many            :flags, :dependent => :destroy
  
  validates_format_of :email,
                      :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i,
                      :message => "should look like an email address",
                      :allow_nil => true,
                      :allow_blank => true
  
  attr_accessible     :login, :email, :password, :password_confirmation
  
  
  def deliver_password_reset_instructions!
    reset_perishable_token!
    Notifier.deliver_password_reset_instructions(self)
  end
    
  def express_errors
    if !self.errors.empty?
      err = Array.new
      if self.errors.on(:login)
        err << ("<b>Login:</b> " + (self.errors.on(:login).is_a?(Array) ? self.errors.on(:login).join(' and ') : self.errors.on(:login)))
      end
      # err << (self.errors.on(:login) ? "Login #{self.errors.on(:login)}" : nil)
      if self.errors.on(:password)
        self.errors.on(:password).is_a?(Array) && self.errors.on(:password).delete("doesn't match confirmation")
        err << ("<b>Password:</b> " + (self.errors.on(:password).is_a?(Array) ? self.errors.on(:password).join(' and ') : self.errors.on(:password)))
      end
      # err << (self.errors.on(:password) ? "Password #{self.errors.on(:password)}" : nil)
      if (self.errors.on(:password_confirmation) && !self.errors.invalid?(:password))
        err << "Password confirmation should match password"
      end
      # err.compact!
      err.join('<br />')
    end
  end
  
  def locilink
    "http://loci.me/u/#{self.login}"
  end
  
end
