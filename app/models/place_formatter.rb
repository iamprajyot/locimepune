class PlaceFormatter < GitemFormatter
  
  include ActionView::Helpers
  
  attr_reader :gitem
  
  def initialize(gitem)
    @gitem = gitem
  end
  
  def xml
    xml = Builder::XmlMarkup.new(:indent=>2)
    xml.instruct! :xml
    xml.locime(:xmlns => "http://loci.me/xml", :version => "1.0", :creator => "loci.me") do
      xml.type(@gitem.gtype)
      xml.url(@gitem.locilink)
      xml.Place do
        xml.name(@gitem.title) unless @gitem.title.blank?
        if !@gitem.description.blank?
          if @gitem.description == h(@gitem.description)
            xml.description(@gitem.description)
          else
            xml.description {
              xml.cdata!(sanitize(@gitem.description))
            }
          end
        end
        xml.latitude(@gitem.lat)
        xml.longitude(@gitem.lon)
        xml.altitude(@gitem.altitude) unless @gitem.altitude.blank?
        xml.link(@gitem.link) unless @gitem.link.blank?
      end
    end
  end
  
  def json
    buf = HashWithIndifferentAccess.new
    buff = HashWithIndifferentAccess.new
    buf[:type]        = @gitem.gtype
    buf[:url]         = @gitem.locilink    
    buff[:name]        = @gitem.title unless @gitem.title.blank?
    buff[:description] = @gitem.description unless @gitem.description.blank?
    buff[:coordinates] = [@gitem.lat, @gitem.lon ]
    buff[:coordinates] << @gitem.altitude unless @gitem.altitude.blank?
    buff[:link]        = @gitem.link unless @gitem.link.blank?
    buf[:Place]       = buff
    buf
  end
  
  def kml
    xml = Builder::XmlMarkup.new(:indent=>2)
    xml.instruct! :xml
    xml.kml(:xmlns => "http://www.opengis.net/kml/2.2") do
      xml.Document {
        xml.Placemark {
          xml.name(@gitem.title)
          xml.description {
            xml.cdata!(sanitize(gitem_description))
          }
          xml.Point {
            xml.coordinates(@gitem.coords_reverse);
          }
        }
      }
    end
  end
  
   # TODO: zjistit, jak se koduje html apod. v gpx - jestli CDATA a nebo entities - ted to mam jako CDATA
  def gpx
    xml = Builder::XmlMarkup.new(:indent=>2)
    xml.instruct! :xml
    xml.gpx(:xmlns => "http://www.topografix.com/GPX/1/1", :version => "1.1", :creator => "loci.me") do
      xml.wpt(:lat => @gitem.lat, :lon => @gitem.lon) {
        xml.name(@gitem.title)
        xml.desc {
          xml.cdata!(sanitize(gitem_description))
        }
      }
    end
  end
  
  def lmx
    xml = ''
    xml << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" 
    xml << "<lm:lmx xmlns:lm=\"http://www.nokia.com/schemas/location/landmarks/1/0\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.nokia.com/schemas/location/landmarks/1/0 lmx.xsd\">\n" 
    xml << "  <lm:landmark>\n"
    xml << "    <lm:name>#{@gitem.title}</lm:name>\n" unless @gitem.title.blank?
    xml << "    <lm:description>#{h(strip_tags(@gitem.description))}</lm:description>\n" unless @gitem.description.blank?
    xml << "    <lm:coordinates>\n"
    xml << "      <lm:latitude>#{@gitem.lat}</lm:latitude>\n"
    xml << "      <lm:longitude>#{@gitem.lon}</lm:longitude>\n"
    xml << "    </lm:coordinates>\n"
    unless @gitem.link.blank?
      xml << "    <lm:mediaLink>\n"
      xml << "      <lm:url>#{@gitem.link}</lm:url>\n"
      xml << "    </lm:mediaLink>\n"
    end
    xml << "  </lm:landmark>\n"
    xml << "</lm:lmx>"
  end
  
  def js
    js_to_render = ''
    js_to_render << "document.write('<span id=\"loci_me_bits\">"
    js_to_render << "<a href=\"#{@gitem.locilink}\" class=\"loci_me_link\" "
    js_to_render << "title=\"See the #{@gitem.gtype.downcase} #{@gitem.ghash}\" target=\"_blank\">"
    js_to_render << (@gitem.title.blank? ? @gitem.ghash : h(@gitem.title).gsub("'", "&#x27;"))
    js_to_render << "</a>"
    js_to_render << "<span class=\"geo\" style=\"display: none\">"
    js_to_render << "<span class=\"latitude\">#{@gitem.lat}</span>,"
    js_to_render << "<span class=\"longitude\">#{@gitem.lon}</span>"
    js_to_render << "</span>"
    js_to_render << "</span>')"
  end
  
  def geomicro(linktext = '')
    "<div class=\"geo\">
      #{linktext}<span class=\"latitude\">#{@gitem.lat}</span>,<span class=\"longitude\">#{@gitem.lon}</span>#{@gitem.altitude.blank? ? '' : ",<span class=\"altitude\">#{@gitem.altitude}</span>"}
    </div>"
  end
  
protected
  
  def gitem_description
    buf = ''
    unless @gitem.description.blank?
      if @gitem.description == h(@gitem.description)
        buf << "<p>#{@gitem.description}</p>\n"
      else
        buf << "#{@gitem.description}\n<br />"
      end
    end
    unless @gitem.link.blank?
      buf << "<a href=\"#{@gitem.link}\" title=\"See more\">See more</a>\n<br />"
    end
    buf << "<a href=\"#{@gitem.locilink}\" title=\"#{@gitem.locilink}\" class=\"ghash\">#{@gitem.locilink}</a>"
    buf
  end
  
  
end