module Gitem
  
  MAX_TITLE_LENGTH = 128
  AVAILABLE_GITEMS = %w{ Place System }
  LATLON_PRECISION = 6
  
  FLAGGED_DEFAULT = false
  HIDDEN_DEFAULT  = false
  
  include Flaggable::FlaggableGitem
  
  # def self.per_page
  #   5
  # end
    
  def self.find_by_ghash(ghash)
    gc = Gcode.find_by_ghash(ghash)
    if gc && gc.gitem
      return gc.gitem
    else
      return nil
    end
  end
    
  def gformat_to
    @formatter ||= PlaceFormatter.new(self)
  end
  
  def share_link
    @sharelinker ||= ShareLinker.new(self)
  end
  
  def mapper
    @mapper ||= Mapper.new(self)
  end
  
  def locilink
    "http://loci.me/#{self.ghash}"
  end

  def locilink_escaped
    "http%3A%2F%2Floci.me%2F#{self.ghash}"
  end
  
  # user cant be changed during session... I do not check it
  # def flagged(userid)
  #   ffu = flags_for_user(userid)
  #   return ( ffu ? ffu.flagged : FLAGGED_DEFAULT )
  # end
  # 
  # def set_flagged(userid, value)
  #   ffu = flags_for_user(userid)
  #   if ffu
  #     ffu.update_attribute(:flagged, value)
  #   else
  #     self.flags.create(:user_id => userid, :flagged => value)
  #   end
  # end
    
  # def editable_by?(user)
  #   # TODO: editable_by?(user) not implemented yet
  #   true
  # end

    
protected

  def flags_for_user(userid)
    @uid ||= userid
    return nil unless @uid
    @ffu ||= self.flags.find(:first, :conditions => {:user_id => @uid})
  end

  # title musi byt multibyte, protoze zkoumame jeho delku a pripadne ho zkracujeme
  # ale mysql adapter to nebere, kdyz to uklada
  def truncate_title
    if self.title
      self.title = ActiveSupport::Multibyte::Chars.new(self.title)
      if self.title.length > MAX_TITLE_LENGTH
        self.title = self.title[0..(MAX_TITLE_LENGTH - 4)] << "..."
      end
      self.title = self.title.to_s      # to je kvuli tomu mysql adapteru
    end
  end

  def sanitize_title
    if self.title
      self.title = sanitize self.title, :tags => []
    end
  end
  
  def reuse_locquery    # To asi nechci ... dela to bordel v tom, co uzivatel zadal
    if self.description.blank? && !self.locquery.blank?
      self.description = self.locquery
    end
  end
  
  def associate_gcode_if_not_exists
    if self.ghash.blank?
      self.build_gcode
      unless self.gcode.valid?
        errors.add_to_base "Cannot create valid Gcode"
        raise ActiveRecord::RecordInvalid, self #, "Not valid gcode"
        return false
      else
        self.ghash = self.gcode.ghash  # pridano pro cachovani ghash u gitemu
      end
    end    
    return true
  end

  def parse_coordinates_string
    if self.cs
      cscopy = cs.dup     #cs.gsub(/\s/,'')
      cscopy.strip!
      typ = nil
      
      case cscopy
      when /^\s*[-.\d]+\s*,\s*[-.\d]+\s*(,\s*-?\d+)?$/
        # @coords_string_type = "WGS84 decimal"
        typ = "WGS84 decimal"
        self.coords = cscopy.gsub(/\s/,'')

      # N 50° 6.179'  E 14° 22.847' or 50° 6.179'N  14° 22.847'E (with or without ', some spaces, ...)
      when /^([NS])?\s*(\d+)°\s*(?:(\d+(?:\.\d*)?)'?)?\s*([NS])?[\s,;]+([EW])?\s*(\d+)°\s*(?:(\d+(?:\.\d*)?)'?)?\s*([EW])?$/i
        typ = "WGS84 degrees decimal minutes"
        latzone = $1 || $4
        lonzone = $5 || $8
        self.lat = ($3.to_f / 60 + $2.to_f)*_zone_flag(latzone)
        self.lon = ($7.to_f / 60 + $6.to_f)*_zone_flag(lonzone)

      # 48°34'10.52" N       18°15'13.93" E
      when /^([NS])?\s*(\d+)°\s*(?:(\d+)')?\s*(\d+(?:\.\d+)?)?\s*\"?\s*([NS])?[\s,;]+([EW])?\s*(\d+)°\s*(?:(\d+)')?\s*(\d+(?:\.\d+)?)?\s*\"?\s*([WE])?$/i
        typ = "WGS84 degrees minutes decimal seconds"
        latzone = $1 || $5
        lonzone = $6 || $10
        self.lat = ($2.to_f + ($3.to_f + $4.to_f / 60) / 60)*_zone_flag(latzone)
        self.lon = ($7.to_f + ($8.to_f + $9.to_f / 60) / 60)*_zone_flag(lonzone)

      end
    end
  end
  
  def parse_loc_query
    gitem_type, gitem_hash = parse_loc_query_string(self.locquery)
    if gitem_type   # WARNING - tady by melo sedet self.class s gitem_type... az budeme mit vice gitem type!
      self.attributes=(gitem_hash)
    end
  end
  
  def round_coords(*coords)
    coords.collect do |c|
      c.to_f.round_with_precision(Gitem::LATLON_PRECISION) if c
    end
  rescue NoMethodError
    nil
  end
  
  def round_coords!(*coords)
    coords.collect! do |c|
      c.to_f.round_with_precision(Gitem::LATLON_PRECISION) if c
    end
  rescue NoMethodError
    nil
  end
    
private
    
  def _zone_flag(flag)
      flag =~ /^(S|W)$/i ? -1 : 1
  end
  
# private :ghash=
    
end
