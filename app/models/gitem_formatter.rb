class GitemFormatter
  
  def initialize(gitem)
    @gitem = gitem
  end
    
  # Tyto metody jsou callback
  # pokud je neimplementuje spravny gitem, pak tyto budou pro vraceni chyby NOT IMPLEMENTED  
  def xml
    "XML for \"#{@gitem.class.name}\" not implemented"
  end
  
  def kml
    "KML for \"#{@gitem.class.name}\" not implemented"
  end
  
  def gpx
    "GPX for \"#{@gitem.class.name}\" not implemented"
  end
  
  def lmx
    "LMX for \"#{@gitem.class.name}\" not implemented"
  end
  
  def js
    "JS for \"#{@gitem.class.name}\" not implemented"
  end
  
  def geomicro
    "Microformat for \"#{@gitem.class.name}\" not implemented"
  end
  
  def self.xml_error(error_text = '', error_code = 400)
    xml = Builder::XmlMarkup.new(:indent=>2)
    xml.instruct! :xml
    xml.locime(:xmlns => "http://loci.me/xml", :version => "1.0", :creator => "loci.me") do
      xml.error {
        xml.text(error_text)
        xml.code(error_code)
      }
    end
  end
  
  def self.json_error(error_text = '', error_code = 400)
    buf = HashWithIndifferentAccess.new
    buf[:type]        = "error"
    buf[:error_text]  = error_text
    buf[:error_code]  = error_code
    buf
  end
  
  def self.js_error(error_text = '', error_code = 400)
    ''
  end
  
  def method_missing(method)
    "#{method} not implemented"
  end
  
end