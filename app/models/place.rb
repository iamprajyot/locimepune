class Place < ActiveRecord::Base
  
  include Gitem
  include ActionView::Helpers
  include Loci::QMatcher
  # velmi osklivy hack ... ale asi az pro dalsi verzi Railsu
  # memoize :flag_for_user
  
  has_one     :gcode, :as => :gitem, :dependent => :destroy
  
  has_many    :flags, :as => :gitem, :dependent => :destroy
                
  # has_one     :address
  
  belongs_to  :user
  
  attr_accessible :lat, :lon, :altitude, :title, :description, :link, :locquery, :coords, :coords_reverse, :cs, :lqs, :acl, :map_provider, :zoom, :maptype
  
  # validates_presence_of     :ghash, :allow_nil => false, :message => "can't be blank"
  validates_presence_of     :lat, :lon, :allow_nil => false, :message => "can't be blank"
  validates_numericality_of :lat, :lon, :message => "has to be a number"
  validates_inclusion_of    :lat,
                            :in => -90..90,
                            :message => "has to be between -90+90"
  validates_inclusion_of    :lon,
                            :in => -180..180,
                            :message => "has to be between -180+180"
  
  validates_format_of       :link,
                            :with => /^((https?|ftps?|smb)\:\/\/.*)?$/i,
                            :message => "has to be valid url"

  validates_numericality_of :zoom,
                            :allow_nil => true,
                            :only_integer => true,
                            :less_than => 30,
                            :greater_than => 0,
                            :message => "has to be a number, 0-30"
  
  validates_length_of       :maptype,
                            :maximum => 15,
                            :allow_nil => true,
                            :allow_blank => true

  before_validation   :sanitize_title, :truncate_title
  after_validation    :add_errors_on_virtual_attr, :rounding_coords
  before_save         :associate_gcode_if_not_exists
  # mel jsem rounding_coords v before_save, ale nevim proc
  
  def self.per_page
    10
  end

  def to_param  # overrides, how are links constructed
    ghash
  end

  def coords
    if self.lat && self.lon
      "#{self.lat},#{self.lon}#{ self.altitude.blank? ? nil : ",#{self.altitude}" }"
    else
      ''
    end
  end

  def coords=(cs)
    if !cs.blank?
      self.lat, self.lon, self.altitude = cs.split(",")
    end
  end

  def coords_reverse
    "#{self.lon},#{self.lat}#{ self.altitude.blank? ? nil : ",#{self.altitude}" }"
  end
  
  def coords_reverse=(cs)
    self.lon, self.lat, self.altitude = cs.split(",")
  end

  def cs
    @coords_string
  end
  
  def cs=(cs)
    @coords_string = cs
    parse_coordinates_string
  end
  
  def lqs=(lqs)
    self.locquery = lqs
    debugger
    # parse_loc_query
    parse_loc_query if !lqs.blank? && self.locquery_changed?
  end
  
  def lqs
    self.locquery
  end
  
  def self.create_or_get_existing(params, user)
    Place.prioritize_params(params)
    place_to_seek = Place.new(params[:place])
    place_to_seek.user = user
    if place_to_seek.valid?
      # delam to tak, protoze se ta data upravuji pred vlozenim do DB a pak by nesedelo porovnani
      place_existing = Place.find(:first,
                                  :conditions => {  :lat => place_to_seek.lat,
                                                    :lon => place_to_seek.lon,
                                                    :altitude => place_to_seek.altitude,
                                                    :title => place_to_seek.title,
                                                    :description => place_to_seek.description,
                                                    :link => place_to_seek.link,
                                                    :acl => place_to_seek.acl,
                                                    :locquery => place_to_seek.locquery,
                                                    :zoom => place_to_seek.zoom,
                                                    :maptype => place_to_seek.maptype,
                                                    :user_id => place_to_seek.user_id,
                                                  })
      # nemelo by spis zoom a maptype updatovat, pokud narazi na stejny...?
      return place_existing, "existing" if place_existing
      rv = place_to_seek.save!
      return place_to_seek, rv
    else
      return place_to_seek, false
    end
  end
   
  def gtype
    "Place"
  end

  def self.looking_for_params
    %w( lat lon altitude coords cs title description link acl )
  end
  
  def self.prioritize_params(params)
    # pokud existuje lat/lon, pak se zahodi coords, cs a lqs
    # pokud existuje coords, zahodi se cs a lqs
    if !params[:place][:lat].blank? && !params[:place][:lon].blank?
      params[:place].delete(:coords)
      tmp = params[:place].delete(:lqs)
      params[:place][:locquery] ||= tmp
    elsif !params[:place][:coords].blank?
      tmp = params[:place].delete(:lqs)
      params[:place][:locquery] ||= tmp
    end
  end
    
      
protected
  
  def rounding_coords
    self.lat, self.lon = round_coords(self.lat, self.lon)
  end
  
  def add_errors_on_virtual_attr
    if self.errors.on(:lat) or self.errors.on(:lon)
      self.errors.add(:coords)
      self.errors.add(:lqs)
      self.errors.add(:cs)
    end
  end

 
end
