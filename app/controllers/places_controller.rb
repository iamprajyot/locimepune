class PlacesController < GitemsController

  def index
    @page_title = "Places listing"
    if current_user
      @places = Place.paginate( :page => params[:page],
                                :conditions => ["user_id = ?", current_user.id],
                                :order => 'updated_at DESC')
      return
    elsif !session[:gitems_created].blank?
      # TODO: zkontrolovat, jestli jsou stale vsechny bez uzivatele!!!
      @places = Place.paginate( :page => params[:page],
                                :conditions => {:id => session[:gitems_created]},
                                :order => 'updated_at DESC')
      @page_h1 = "Places you've made in this session"
      return
    else
      @places = nil
    end
    # @places = Place.paginate(:page => params[:page], :conditions => ["user_id = ?", current_user.id], :order => 'updated_at DESC')
  end

end
