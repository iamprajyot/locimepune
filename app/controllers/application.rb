# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base
  include HoptoadNotifier::Catcher
  include Loci::QMatcher
  helper :all # include all helpers, all the time
  # authlogic stuff
  filter_parameter_logging :password, :password_confirmation
  helper_method :current_user_session, :current_user

  # See ActionController::RequestForgeryProtection for details
  # Uncomment the :secret if you're not using the cookie session store
  protect_from_forgery :secret => '6844e4234cdbfe4c4efd31ec3fc002e2'
  
  # See ActionController::Base for details 
  # Uncomment this to filter the contents of submitted sensitive data parameters
  # from your application log (in this case, all fields with names like "password"). 
  # filter_parameter_logging :password
  
private
  
    
  
  def current_user_session
    return @current_user_session if defined?(@current_user_session)
    @current_user_session = UserSession.find
  end

  def current_user
    return @current_user if defined?(@current_user)
    @current_user = current_user_session && current_user_session.user
  end
  
  def require_user
    unless current_user
      store_location
      flash[:warn_full] = "You must be logged in to access this page"
      redirect_to new_user_session_url
      return false
    end
  end

  def require_no_user
    if current_user
      store_location
      flash[:warn_full] = "You must be logged out to access this page"
      redirect_to account_url
      return false
    end
  end
  
  def log_in_before
    store_location
    flash[:warn_full] = "You must be logged in to access this page"
    redirect_to new_user_session_url
    return true
  end

  def store_location
    session[:return_to] = request.request_uri
  end

  def redirect_back_or_default(default)
    if current_user && session[:gitems_created] && session[:gitems_created].is_a?(Array) && !session[:gitems_created].blank?
      # redirect_to page_where_can_agree_to_ad_previously_created_places_to_profile_and_delete_this_session_data
      # return
    end
    redirect_to(session[:return_to] || default)
    session[:return_to] = nil
  end
  
  
end
