class PagesController < ApplicationController
  
  layout  "default"
  
  def index
    # render :text => "index"
  end
  
  def pager
    page_to_render = params[:pagespath] ? File.join(params[:pagespath]) : "error_404"
    render :action => page_to_render       #File.join(params[:pagespath])
  rescue ActionView::MissingTemplate
    render :action => "error_404"
  end
  
  def error_404
    
  end
  
private
  
  
  
end
