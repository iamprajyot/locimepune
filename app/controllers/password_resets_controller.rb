class PasswordResetsController < ApplicationController
  
  before_filter :require_no_user
  before_filter :load_user_using_perishable_token, :only => [:edit, :update]
  
  layout "default"
  
  def new
    render
  end
  
  def edit
    render
  end

  def create
    if !params[:email].blank? && params[:email] =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
      @user = User.find_by_email(params[:email])
      if @user
        @user.deliver_password_reset_instructions!
        flash[:notice_full] = "Instructions to reset your password have been emailed to you. " +
          "Please check your email."
        redirect_to root_url
        return
      else 
        flash[:warn_full] = "No user was found with that email address"
        render :action => :new
      end
    else
      flash[:warn_full] = "You have to submit valid email address."
      render :action => :new
    end
  end
  
  def update
    @user.password = params[:user][:password]
    @user.password_confirmation = params[:user][:password_confirmation]
    if @user.save
      flash[:notice_full] = "Password successfully updated"
      redirect_to account_url
    else
      render :action => :edit
    end
  end
  
private
  
  def load_user_using_perishable_token
    @user = User.find_using_perishable_token(params[:id], 30.minutes)  # toto jsem pridal ... ten cas
    unless @user
      flash[:notice_full] = "We're sorry, but we could not locate your account. " +
        "If you are having issues try copying and pasting the URL " +
        "from your email into your browser or restarting the " +
        "reset password process."
      redirect_to new_password_reset_path
    end
  end
  
end
