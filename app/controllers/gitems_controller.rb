class GitemsController < ApplicationController
  
  # include Gitem
  # include Loci::QMatcher
  
  before_filter :redirect_to_mobile_action_if_mobile, :only => :show
  before_filter :ua, :only => :show
  before_filter :set_controller_context
        
  helper_method :gitem_can_be_edited?
  
  layout "default"
  
  # toto bude pro listing gitemu! (casem)
  def index
    render :action => :new #, :layout => "default_form"
    # @places = Place.paginate(:page => params[:page], :order => 'updated_at DESC')
  end
  
  def q_new
    # render :layout => "default_form"
  end
  
  def q_create
    gitem_type, gitem_hash = parse_loc_query_string(params[:q])

    unless gitem_type
      logerror("q_create_errors_in_data")
      render_error(:error_code => 422, :error_message => "Ooops? What?", :render => {:action => :q_new})
    else
      params[gitem_type].is_a?(Hash) ? params[gitem_type].reverse_merge!(gitem_hash) : params[gitem_type] = gitem_hash
      if params[gitem_type][:title].blank?
        params[gitem_type][:title] = params[gitem_type][:locquery]
      end
      @controller_context = gitem_type.to_s.pluralize
      create
      return true
    end
  end
    
  def new
    # render :layout => "default_form"
  end
  
  def show
    params[:id] ||= params[:ghash]
    get_gitem_from_params || (render_error_unless_gitem_not_found && return)
    gitem_can_be_viewed? || (log_in_before && return)
    pfx = params[:view_for] ? params[:view_for] + '_' : ''
    render_ok(:render => {:layout => pfx + "default", :action => pfx + "show"})
  end
      
  def create
    gitem_type_class = @controller_context.singularize.camelize.constantize
    unflatten_params!(gitem_type_class) if params[:apicall] # that's for api call
    @gitem, status = gitem_type_class.create_or_get_existing(params, current_user)
    unless status
      logerror("gitem_create_errors_in_data")
      render_error(:error_code => 422, :error_message => "Ooops? What?", :render => {:action => :new}) #, :layout => "default_form"})
      return
    end
    add_gitem_to_session unless current_user && status != "existing"
    render_ok(:message => (status == "existing" ? "Existed" : "Created"), :redirect_to => ghash_url(@gitem.ghash))
    return
  end
  
  def edit
    get_gitem_from_params || (render_error_unless_gitem_not_found && return)
    render_error_unless_gitem_not_editable && return
    # render :layout => "default_form"
  end
  
  def update
    get_gitem_from_params || (render_error_unless_gitem_not_found && return)
    render_error_unless_gitem_not_editable && return
    @gitem.class.prioritize_params(params)
    if @gitem.update_attributes(params[@gitem.gtype.downcase])
      render_ok(:message => "#{@gitem.gtype} updated!", :redirect_to => ghash_url(@gitem.ghash))
    else
      logerror("gitem_update_errors_in_data")
      render_error(:error_code => 422, :error_message => "Data not valid :(", :render => {:action => :edit}) #, :layout => "default_form"})
    end  
  end
    
  # predpokladam v params a spol.: gitem.ghash, flag[:type] = value, current_user
  def set_flag_on_gitem_for_current_user
    if current_user
      if params[:flag].is_a?(Hash)
        if (@gitem || get_gitem_from_params)
          if gitem_can_be_viewed?()  # zatim jen vraci true!
            if @gitem.flag(current_user).update_attributes(params[:flag])
              # all ok
              # pro kazdy parametr, co chce ulozit, zjistime, jestli byl zmeneny
              # replace icon
              return
            else
              #error "Cannot update - LOGME"
            end
          else
            #error "This gitem is not available"
          end
        else
          #error "Place not found"
        end
      else
        #error "I do not know what you want to do"
      end
    else
      #error "You must be logged in to do this"
    end
    # render error
    return
  end    

# ==============================================================================
protected
  
  def get_gitem_from_params
    @gitem = Gitem.find_by_ghash(params[:id])
  end
  
  def render_error_unless_gitem_not_found
    logerror("gitem_not_found")
    render_error(:error_code => 400, :error_message => "The link seems incorrect.", :redirect_to => root_url)
    true
  end
  
  def render_error_unless_gitem_not_editable
    unless gitem_can_be_edited?
      logerror("gitem_not_editable")
      render_error(:error_code => 403, :error_message => "You do not have rights to edit this place.", :redirect_to => ghash_url(@gitem.ghash))
      return true
    end
  end
   
  def valid_gitem_type?(g)
    Gitem::AVAILABLE_GITEMS.include?(g.to_s.downcase.camelize)
  end
  
  def unflatten_params!(gtype)
    p = HashWithIndifferentAccess.new
    at = gtype.looking_for_params
    params.each_pair do |key, val|
      p[key] = val if at.include?(key)
    end
    g = gtype.to_s.downcase
    params[g] = p
  end
    
  def render_error(options = {})
    options.reverse_merge!(:error_code => 400, :error_message => "Such page does not exist.")
    respond_to do |format|
      format.html { 
        case
        when options.has_key?(:render)
          flash.now[:warn] = options[:error_message]
          render_in_context(options[:render])
        when options.has_key?(:redirect_to)
          flash[:warn] = options[:error_message]
          redirect_to options[:redirect_to]
        else
          flash.now[:warn] = options[:error_message]
          render_in_context
        end
      }
      format.xml  { render :text => GitemFormatter.xml_error(options[:error_message],  options[:error_code]), :layout => false, :status => options[:error_code] }
      format.kml  { render :text => GitemFormatter.xml_error(options[:error_message],  options[:error_code]), :layout => false, :status => options[:error_code] }
      format.gpx  { render :text => GitemFormatter.xml_error(options[:error_message],  options[:error_code]), :layout => false, :status => options[:error_code] }
      format.lmx  { render :text => GitemFormatter.xml_error(options[:error_message],  options[:error_code]), :layout => false, :status => options[:error_code] }
      format.json { render :json => GitemFormatter.json_error(options[:error_message], options[:error_code]), :layout => false, :status => options[:error_code] }
    end
    return
  end
  
  def render_ok(options={})
    respond_to do |format|
      format.html { 
        case
        when options.has_key?(:render)
          flash.now[:notice] = options[:message] if options[:message] && flash[:notice].blank?
          render_in_context(options[:render])
        when options.has_key?(:redirect_to)
          flash[:notice] = options[:message] if options[:message] && flash[:notice].blank?
          redirect_to options[:redirect_to]
        else
          flash[:notice] = options[:message] if options[:message] && flash[:notice].blank?
          render_in_context
        end
      }
      format.xml  { render :text => @gitem.gformat_to.xml,  :layout => false }
      format.kml  { render :text => @gitem.gformat_to.kml,  :layout => false }
      format.gpx  { render :text => @gitem.gformat_to.gpx,  :layout => false }
      format.lmx  { render :text => @gitem.gformat_to.lmx,  :layout => false }
      format.json { render :json => @gitem.gformat_to.json, :layout => false }
      format.js   { render :js   => @gitem.gformat_to.js }
    end
    return
  end
  
  def add_gitem_to_session
    if !session[:gitems_created] || !session[:gitems_created].is_a?(Array)
      session[:gitems_created] = Array.new
    end
    session[:gitems_created] << @gitem.id
  end
  
  def gitem_can_be_edited?
    gitem_created_by_current_user? || gitem_created_in_current_anon_session?
    # (current_user && @gitem.user && current_user.id == @gitem.user.id) || (session[:gitems_created] && session[:gitems_created].include?(@gitem.id))
  end
  
  def gitem_can_be_viewed?
    #nothing now TODO:
    true
  end
  
  def gitem_can_be_commented?
    #nothing now
    true
  end
  
  def gitem_can_be_rated?
    #nothing now
    true
  end
  
  def gitem_created_by_current_user?
    current_user && @gitem.user_id && current_user.id == @gitem.user_id
    # toto fungovalo, ale delalo to o dotaz do DB vice
    # current_user && @gitem.user.id && current_user.id == @gitem.user.id
  end
  
  def gitem_created_in_current_anon_session?
    session[:gitems_created] && session[:gitems_created].include?(@gitem.id)
  end
  
  def redirect_to_mobile_action_if_mobile
    @ua = UserAgent.new(request)
    if @ua.is_mobile? && !params.has_key?("!") && params[:view_for] != "mobile" && params[:format].blank?
      redirect_to ghash_mobile_url(params[:ghash])
    end
  end
       
  def ua
    @ua = UserAgent.new(request)
  end
  
  def logerror(name, opts = {})
    data_to_save = {  :name => name,
                      :vals => {:params => params,
                                :user => (current_user ? current_user.id : nil)}
                    }
    data_to_save[:vals].merge!(opts) unless opts.blank?
    # Logerror.create(  :name => name,
    #                   :vals => {:params => params,
    #                             :user => (current_user ? current_user.id : nil)})
    Logerror.create(data_to_save)
  end
  
  def set_controller_context
    @controller_context ||= params[:controller]
  end
  
  def render_in_context(options = {})
    act = (options.delete(:action) || params[:action]).to_s
    template = (@controller_context ? @controller_context + "/" : '') + act
    options.reverse_merge!(:template => template)
    render options
  end
        
end
