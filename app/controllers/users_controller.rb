class UsersController < ApplicationController
  
  before_filter :require_no_user, :only => [:new, :create]
  before_filter :require_user, :only => [:show, :edit, :update]
  
  layout "default"
  
  def index
    redirect_to root_url
  end

  def new
    @user = User.new
  end

  def create
    params[:user][:login].downcase!
    @user = User.new(params[:user])
    if @user.save
      flash[:notice] = "You are registered!"
      redirect_back_or_default account_url
    else
      flash[:warn] = "Data not valid :("
      flash[:warn_full] = @user.express_errors
      render :action => :new
    end
  end

  def show
    @user = @current_user
  end

  def edit
    @user = @current_user
  end

  def update
    @user = @current_user # makes our views "cleaner" and more consistent
    params[:user].delete(:login) # login is unchangeable!
    if @user.update_attributes(params[:user])
      flash[:notice] = "Updated!"
      redirect_to account_url
    else
      flash[:warn] = "Data not valid :("
      flash[:warn_full] = @user.express_errors
      render :action => :edit
    end
  end
  
  
end
