# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  
  def page_title
    if @gitem && @gitem.id
      "#{@gitem.title.blank? ? 'Place to see...' : @gitem.title} (by loci.me)"
    elsif @page_title
      "#{@page_title} (loci.me)"
    else
      "Loci.me - Links to places"
    end
  end

  def flash_message(opts = {})
    opts.reverse_merge!(:what => [:notice, :warn])
    if flash.empty?
      return ''
    else
      buf = ''
      flash.each_key do |type|
        if opts[:what].include?(type)
          message = flash.delete(type)
          buf << "<div class=\"#{type}\">#{message}</div>" unless message.blank?
        end
      end
      unless buf.blank?
        buf = '<div id="flash">' + buf + '</div>'
        # buf << '<div class="clearer">&nbsp;</div>'
      end
      return buf
    end
  end
  
  def head_items
    buf = ''
    if @ua && @ua.browser_family == "iphoneipod"
      buf <<  "<meta name=\"viewport\" content=\"width=320, initial-scale=1.0, user-scalable=yes\">\n"
      buf <<  "<script type=\"application/x-javascript\">\n"
      buf <<  "addEventListener(\"load\", function() { setTimeout(hideURLbar, 0); }, false);\n"
      buf <<  "function hideURLbar(){window.scrollTo(0,1);}\n"
      buf <<  "</script>"
    end
  end
  
  def sms_to(phone_number, name = nil, html_options = {})
    return '' if @ua && @ua.browser_family == "iphoneipod"
    html_options = html_options.stringify_keys
    body = html_options.delete("body")
    number = html_options.delete("number")

    number = number ? number.gsub!(/[^\d\+]/,"") : ''
    extras = body.nil? ? '' : "?body=#{CGI.escape(body).gsub("+", "%20")}"

    content_tag "a", name, html_options.merge({ "href" => "sms:#{number}#{extras}" })
  end
  
  
end
