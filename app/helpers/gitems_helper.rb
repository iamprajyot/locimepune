module GitemsHelper
  
  def edit_link_if_can_edit(txt="edit")
    gitem_can_be_edited? ? "<div class=\"edit_item\">#{link_to txt, edit_place_url(@gitem.ghash)}</div>" : ''
  end
  
  def edit_link_visible_if_can_edit(txt="edit")
    gitem_can_be_edited? ? link_to( txt, edit_place_url(@gitem.ghash)) : txt
  end
  
  def links_to_formats(attrs={})
    buf = ''
    is_first = 0
    formats = attrs[:formats] || ["kml", "gpx", "lmx"]
    formats.each do |format|
      buf << attrs[:separator] unless is_first == 0
      buf << "<span>"
      buf << link_to(format, ghash_format_url(:ghash => @gitem.ghash, :format => format))
      buf << "</span>"
      is_first += 1
    end
    return buf
  end
  
  def links_to_show_on_maps(attrs={})
    buf = ''
    is_first = 0
    maps = attrs[:maps] || { :gm => "google map", :ym => "yahoo map"}
    maps.each_pair do |map, text|
      buf << attrs[:separator] unless is_first == 0
      buf << "<span>"
      buf << link_to(text, @gitem.share_link.send(map, params[:view_for]))
      buf << "</span>"
      is_first += 1
    end
    return buf
  end
  
  def static_map_image_tag(attrs={})
    image_tag(  @gitem.mapper.static.display(attrs),
                :id => "mapdata",
                :alt => h(@gitem.title) || "Place to see...",
                :title => h(@gitem.title) || "Place to see...",
                :size => @gitem.mapper.static.mapsize(attrs).join('x'))
  end
  
  
end
